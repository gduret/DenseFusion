#!/bin/bash

set -x
set -e

export PYTHONUNBUFFERED="True"
export CUDA_VISIBLE_DEVICES=0

python3 ./tools/eval_linemod.py --dataset_root ./datasets/linemod/Linemod_preprocessed\
  --model trained_models/linemod8/pose_model_4_0.012983739659874712.pth --refine_model trained_models/linemod8/pose_refine_model_9_0.01186443073513208.pth
